﻿mvcApp = angular.module('myApp',[]);



(function () {
    mvcApp.controller('ngCtrl_main', function ($scope,$http) {

        $scope.users = []       // naudojamas irasu atvazdavimui ekrane
        $scope.new = false;         // naudojamas identifikuoti, ar siuo metu ivedamas naujas irasas
        $scope.edit = false;        // naudojamas identifikuoti, ar siuo metu redaguojamas irasas
        $scope.rec = {};           // naudojamas iraso redagavimo arba naujo ivedimo duomenims
        $scope.errName = false;    // naudojamas vardo validacijai
        $scope.errLastName = false;    // naudojamas pavardes validacijai
        $scope.errEmail = false;       // naudojamas naudojamas elektroninio pasto validacijai
        $scope.errPhoneNr = false;     // naudojamas telefono numerio validcijai
        $scope.errInput = false;        // bet vienas laukas neteisingai ivestas

        //gauna visus asmenis ir ideda i masyva.
        $http.get('http://localhost:3000/users').then(function(successCallback){
          for(var i=0; i<successCallback.data.length; i++){
              $scope.users.push(successCallback.data[i]);
          }
        });

        // istrina pasirinkta irasa
        $scope.removeUser = function(index){
            if($scope.edit) return; //jeigu irasas yra koreguojamas, trinimas - negalimas
            $http({
                method: 'Delete',
                url: 'http://localhost:3000/users/' + $scope.users[index].id
              }).then(function successCallback(response) {
                    $scope.users.splice(index,1);
                });
        }

        // index - iraso vieta masyve
        $scope.editUser = function(index) {
            $scope.edit = true;
            $scope.new = false;
            $scope.rec.firstname = $scope.users[index].firstname;
            $scope.rec.lastname = $scope.users[index].lastname;
            $scope.rec.email = $scope.users[index].email;
            $scope.rec.phone = $scope.users[index].phone;
            $scope.index = index;
        }

        // atidaromas pridejimo/koregavimo langas pridejimo rezimu.
        $scope.add = function(){
            $scope.edit = false;
            $scope.new = true;
            $scope.rec.firstname = '';
            $scope.rec.lastname = '';
            $scope.rec.email = '';
            $scope.rec.phone = '';
            $scope.index = -1;
        }

        //Funkcija uzdaro ir istrina koregavimo/pridejimo langa
        $scope.cancel = function(){
            $scope.edit = false; 
            $scope.new = false;
        }

        $scope.update = function(user){
            $scope.errInput = false;
            // vardo validacija
            if ( !$scope.rec.firstname ) { $scope.errName = true; $scope.errInput = true; } else { $scope.errName = false; }
            // pavardes validacija
            if ( !$scope.rec.lastname ){ $scope.errLastName = true; $scope.errInput = true; } else { $scope.errLastName = false; }
            // email validacija
            if ( !$scope.rec.email ){ $scope.errEmail = true; $scope.errInput = true; } else { $scope.errEmail = false; }
            // telefono numerio validacija
            if ( !$scope.rec.phone ){ $scope.errPhoneNr = true; $scope.errInput = true; } else { $scope.errPhoneNr = false; }
            // funkcijos nutraukimas, jei bent vienas laukas nepraeina validacijos
            if ( $scope.errInput ) { return; }
   
            $http({
                method: 'Delete',
                url: 'http://localhost:3000/users/' + $scope.users[$scope.index].id
            }).then(function successCallback(response) {
                    $http({
                        method: 'Post',
                        url: 'http://localhost:3000/users/',
                        data: {
                            phone: $scope.rec.phone,
                            email: $scope.rec.email, 
                            lastname: $scope.rec.lastname, 
                            firstname: $scope.rec.firstname,
                            id: $scope.users[$scope.index].id
                        }
                    }).then(function successCallback(response) {
                            $scope.refreshData();
                            $scope.edit = false;
                        });
                });
        }

        $scope.insert = function(){
            $scope.errInput = false;
            // vardo validacija
            if ( !$scope.rec.firstname ) { $scope.errName = true; $scope.errInput = true; } else { $scope.errName = false; }
            // pavardes validacija
            if ( !$scope.rec.lastname ){ $scope.errLastName = true; $scope.errInput = true; } else { $scope.errLastName = false; }
            // email validacija
            if ( !$scope.rec.email ){ $scope.errEmail = true; $scope.errInput = true; } else { $scope.errEmail = false; }
            // telefono numerio validacija
            if ( !$scope.rec.phone ){ $scope.errPhoneNr = true; $scope.errInput = true; } else { $scope.errPhoneNr = false; }
            // funkcijos nutraukimas, jei bent vienas laukas nepraeina validacijos
            if ( $scope.errInput ) { return; }

            //iraso informacija, kuri bus prideta.
            var data ={
                firstname: $scope.rec.firstname,
                lastname: $scope.rec.lastname,
                email: $scope.rec.email,
                phone: $scope.rec.phone
            }
            $http({
                method: 'Post',
                url: 'http://localhost:3000/users/',
                data: data
            }).then(function successCallback(response) {
                    $scope.refreshData();
                    $scope.new = false;
                });
        }

        //Atnaujinam duomenis, kad zinotume naujo iraso id, kuris yra automatiskai sukuriamas backende
        $scope.refreshData = function(){
            $scope.users = [];
            $http.get('http://localhost:3000/users').then(function(successCallback){
                for(var i=0; i<successCallback.data.length; i++){
                    $scope.users.push(successCallback.data[i]);
                }
              });
        }
    });
//----------------------------------------------------------
    mvcApp.run([function () { 
      console.log('run started!');
    }]);
})();